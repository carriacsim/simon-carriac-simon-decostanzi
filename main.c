#include <stdlib.h>
#include <stdio.h>
#include "info.h"

int main(int argc,char* argv[]){
    int** kernel;
    kernel=malloc(sizeof(int*)*5);
    for (int i=0; i<5;i++){
        kernel[i]=malloc(sizeof(int)*5);
    }
    for(int i=0;i<5;i++){
        for(int j=0;j<5;j++){
        kernel[i][j]=0;
        }
    }
    kernel[0][2]=1;kernel[1][2]=1;kernel[2][2]=1;kernel[3][2]=1;kernel[4][2]=1;kernel[2][0]=1;kernel[2][1]=1;kernel[2][3]=1;kernel  [2][4]=1;
    int** filtre;
    filtre=malloc(sizeof(int*)*3);
    for (int i=0; i<3;i++){
        filtre[i]=malloc(sizeof(int)*3);
    }
    if (argv[1]=="-h"){
        printf("-b seuil : binarise une image ;`\n -c : réalise un renforcement de contraste ;  \n  -d : réalise une dilatation ; \n -e : réalise une érosion ;   \n-f : réalise un flou ; -i fichier : définit le fichier d’entrée, OBLIGATOIRE (sauf si -h utilisée) ; \n -l : réalise une détection de contours/lignes ; \n -g : convertit l’image en niveau de gris ; \n -o fichier : définit le fichier de sortie ; \n-r : réalise un recadrage dynamique ; ");
        return 0;
    }else{
        image img=chargerImage(argv[1]);
        image img2=copier(img);
        if (argv[2]=="-b"){
            int seuil=atoi(argv[3]);
            binarisation(seuil, img);
        }
        else if (argv[2]=="-c"){
            filtre[0][0]=0;filtre[0][1]=-1;filtre[0][2]=0;filtre[1][0]=-1;filtre[1][1]=5;filtre[1][2]=-1;filtre[2][0]=0;filtre[2][1]=-1;filtre[2][2]=0;
            img2=convolution(img, filtre);
        }
        else if (argv[2]=="-d"){
            dilatation(img, kernel);
        }
        else if (argv[2]=="-e"){
            erosion(img, kernel);
        }
        else if (argv[2]=="-l"){
            filtre[0][0]=-1;filtre[0][1]=-1;filtre[0][2]=-1;filtre[1][0]=-1;filtre[1][1]=8;filtre[1][2]=-1;filtre[2][0]=-1;filtre[2][1]=-1;filtre[2][2]=-1;
            img2=convolution(img, filtre);
        }
        else if (argv[2]=="-g"){
            niveauGris(img);
        }
        else if (argv[2]=="-r"){
            recadrage(img);
        }
        else if (argv[2]=="-x"){
            int largeur, hauteur, epaisseur;
            largeur=atoi(argv[3]);hauteur=atoi(argv[4]);epaisseur=atoi(argv[5]);
            creationImage(largeur,hauteur,epaisseur);
        }
        return 0;
    }
} 

