#include <stdlib.h>
#include <stdio.h>
#include "info.h"

/* Auteur : Simon Decostanzi
 Date :   24/05/22
 Résumé : creer un objet de type image a partir d'un fichier .ppm
 Entrée(s) : char* nomFichier
 Sortie(s) :  image
*/
image chargerImage(char* nomFichier){
  FILE* monImage = NULL;
  char ligne[N];
  monImage = fopen(nomFichier, "r");//nomFichier doit contenir le .ppm
  image img;
  if (monImage == NULL) {
    printf("Erreur ouverture fichier \n");
  }else{
    img.formatType=malloc(sizeof(char)*2);// car on sait que c soit "P3" soit "P6". On aurai pu juste mettre image.formatType="P3" mais c plus pro
    img.formatType="P3";
    fgets(ligne,N,monImage);
    fgets(ligne,N,monImage);
    img.largeur=atoi(strtok(ligne," "));
    img.hauteur=atoi(strtok(ligne," "));
    fgets(ligne,N,monImage);
    img.luminance=atoi(ligne);
    //initialiser le tableau de pixels
    img.tabPixel=malloc(sizeof(pixel*)*img.hauteur);
    for (int i=0; i<img.hauteur;i++){
      //on met l'alocation des lignes ici pour economiser des ressources, mm si c'est pas exactement comme dans le cours
      img.tabPixel[i]=malloc(sizeof(pixel)*img.largeur);
      for (int j=0; j<img.largeur;j++){
        //un pixel represente 3 lignes, chaque ligne est une des couleurs
        fgets(ligne,N,monImage);
        img.tabPixel[i][j].r=atoi(ligne);
        fgets(ligne,N,monImage);
        img.tabPixel[i][j].g=atoi(ligne);
        fgets(ligne,N,monImage);
        img.tabPixel[i][j].b=atoi(ligne);
        //la repetition est redondante mais simple et efficace
      }
    }
  }
  return img;
}

/* Auteur : Simon Decostanzi
 Date :   26/05/22
 Résumé : creer un objet de type image qui correspond a une croix et le stock dans un fichier .ppm grace a la fonction sauvegarde
 Entrée(s) : char* nomFichier, la longueur et hauteur de l'image, et l'epaisseur de la croix
 Sortie(s) :  renvoi 1 si l'image a bien été sauvegardée, 0 sinon
*/

image creationImage(char* nomFichier, int x, int y, int width){
  image imageCroix;
  imageCroix.formatType=malloc(sizeof(char)*2);
  imageCroix.formatType="P3";
  imageCroix.largeur=x;imageCroix.hauteur=y;
  imageCroix.luminance=255;
  imageCroix.tabPixel=malloc(sizeof(pixel*)*imageCroix.hauteur);
  int dimensionCroix=MIN(x,y)/2;

  for(int i=0; i<y;i++){
    imageCroix.tabPixel[i]=malloc(sizeof(pixel)*imageCroix.largeur);
    for(int j=0; j<x;j++){
      if((abs(j-x/2)<=width/2 && abs(i-y/2)<=dimensionCroix) || (abs(j-x/2)<=dimensionCroix  && abs(i-y/2)<=width/2)){
        //la coix est formée de 2 rectangles. si on est dans le rectangle verticale ou le rectangle horizontale, on est dans la croix
        imageCroix.tabPixel[i][j].r=255; imageCroix.tabPixel[i][j].g=255; imageCroix.tabPixel[i][j].b=255;
      }else{
        imageCroix.tabPixel[i][j].r=0; imageCroix.tabPixel[i][j].g=0; imageCroix.tabPixel[i][j].b=0;
      }
    }
  }
  return imageCroix;
}

/* Auteur : Simon Decostanzi
 Date :   30/05/22
 Résumé : modifie une struct image, et rend l'image en niveaux de gris
 Entrée(s) : l'image a modifier
 Sortie(s) :
*/
void niveauGris(image img){
  int luminanceGris;
  int r,g,b;
  for(int i=0; i<img.hauteur;i++){
    for(int j=0; j<img.largeur;j++){
      r=img.tabPixel[i][j].r; g=img.tabPixel[i][j].g; b=img.tabPixel[i][j].b;
      luminanceGris=round(0.2126  * r + 0.7152   * g + 0.0722   * b );
      img.tabPixel[i][j].r=luminanceGris;
      img.tabPixel[i][j].g=luminanceGris;
      img.tabPixel[i][j].b=luminanceGris;
    }
  }
}


/* Auteur : Simon Decostanzi
 Date :   30/05/22
 Résumé : modifie une struct image, et rend l'image en noir ou blanc en fonction d un seuil: tous les pixels qui ont un niveau de gris au dessus du
 seuil deviennent blancs, et ceux en dessous du seuil sont noirs
 Entrée(s) : l'image a modifier
 Sortie(s) :
*/
void binarisation(int seuil, image img){
  int luminanceGris;
  int r,g,b;
  for(int i=0; i<img.hauteur;i++){
    for(int j=0; j<img.largeur;j++){
      r=img.tabPixel[i][j].r; g=img.tabPixel[i][j].g; b=img.tabPixel[i][j].b;
      luminanceGris=round(0.2126  * r + 0.7152   * g + 0.0722   * b );
      if (luminanceGris>=seuil){
        img.tabPixel[i][j].r=255;
        img.tabPixel[i][j].g=255;
        img.tabPixel[i][j].b=255;
      }else{
        img.tabPixel[i][j].r=0;
        img.tabPixel[i][j].g=0;
        img.tabPixel[i][j].b=0;
      }
    }
  }
}

/* Auteur : Simon carriac
 Date :   24/05/2022
 Résumé : sauvegarde une image sous format .ppm
 Entrée(s) : une structure image et le nom du fichier à sauvergarder
 Sortie(s) :  1 si succès de la sauvergarde, 0 sinon
*/

int sauvegarde (image img, char* nom){
  FILE* sauvegarde=NULL;
  //sauvegarde=fopen("%s","w",nom);
  sauvegarde=fopen(nom,"w"); //"nom" c est juste un mot, c est pas le fichier qui s appel nom
  //ligne originale: sauvegarde=fopen(“nom”,"w"); 
  //échec d'ouverture du fichier
  if (sauvegarde==NULL){
    printf ("Erreur, la sauvegarde a échouée");
    return 0;
  }else{//qd on met un return dans un if il faut faire un else et mettre un autre return dedans
    fprintf(sauvegarde, "P3 \n");
    fprintf(sauvegarde,"%d %d \n",img.largeur,img.hauteur);
    fprintf(sauvegarde,"%d \n",img.luminance);
    for (int i=0;i<img.hauteur;i++){
      for (int j=0;j<img.largeur;j++){
        fprintf(sauvegarde,"%d \n",img.tabPixel[i][j].r);
        fprintf(sauvegarde,"%d \n",img.tabPixel[i][j].g);
        fprintf(sauvegarde,"%d \n",img.tabPixel[i][j].b);

      }
    }
    return 1;
  }

}
/* Auteur : Simon carriac
 Date :   05/06/2022
 Résumé : inverse la gauche et la droite d'une image
 Entrée(s) : une structure image 
 Sortie(s) :  
*/
void miroir (image img){
    int i;
    int j;
    pixel tmp;
    for (int k=0;k<img.hauteur;k++){
        i=0;
        j=img.largeur;
        while (i<j){
            tmp=img.tabPixel[k][i];
            img.tabPixel[k][i]=img.tabPixel[img.hauteur-k][j];
            img.tabPixel[img.hauteur-k][j]=tmp;
            i++;
            j--;
            
        }
    }
}
/* Auteur : Simon carriac
 Date :   05/06/2022
 Résumé : fais un histogramme d'une image en niveau de gris contenant en indice la valeur de la luminescence
  et en valeur le nombre d'occurences dans l'image
 Entrée(s) : une structure image 
 Sortie(s) :  un tableau contenant l'histogramme en question
*/
int histogramme (image img){
    int Tab[255];
    for (int i=0; i<256;i++){
        Tab[i]=0;
    }
    for(int i=0; i<img.hauteur;i++){
        for(int j=0; j<img.largeur;j++){
            Tab[img.tabPixel[i][j].r]++;
            
        }
    }
    return Tab[255];
}
/* Auteur : Simon carriac
 Date :   05/06/2022
 Résumé : améliore le contraste d'une image en répartissant mieux les composantes de cette image
 Entrée(s) : une structure image 
 Sortie(s) :  
*/
void recadrage (image img){
  image image1;
  int* histo;
  int min=img.largeur*img.hauteur;
  int minI=0;
  int max=0;
  int maxI=0;
  int delta;
  histo=malloc(260*sizeof(int));
  image1=niveauGris(img);
  histo=histogramme(image1);
  histo[3]=128;
  for (int x=0;x<256;x++){
    //max
    if (max<histo[x]){
      max=histo[x];
      maxI=x;
    }
    //min
    if ((histo[x]<min)&& (histo[x]!=0)){
      printf("lol  %d  %d   \n",histo[x],min);
      min=histo[x];
      minI=x;
    }
  }
  delta=255/(maxI-minI);

  for(int i=0; i<img.hauteur;i++){
    for(int j=0; j<img.largeur;j++){
	    img.tabPixel[i][j].r=(img.tabPixel[i][j].r-min)*delta;
	    img.tabPixel[i][j].b=(img.tabPixel[i][j].b-min)*delta;
	    img.tabPixel[i][j].g=(img.tabPixel[i][j].g-min)*delta;
     }
  }

}

/* Auteur : Simon Decostanzi
 Date :   30/05/22
 Résumé : fait le negatif de chaque couleurs de chaque pixels d'une struct image (255 -  couleur)
 Entrée(s) : l'image a modifier
 Sortie(s) :
*/
void negatif(image img){
  int luminanceMax=img.luminance;
  for(int i=0; i<img.hauteur;i++){
    for(int j=0; j<img.largeur;j++){
      img.tabPixel[i][j].r=luminanceMax-img.tabPixel[i][j].r;
      img.tabPixel[i][j].g=luminanceMax-img.tabPixel[i][j].g;
      img.tabPixel[i][j].b=luminanceMax-img.tabPixel[i][j].b;
    }
  }
}


/* Auteur : Simon Decostanzi
 Date :   3/06/22
 Résumé : fait la convolutiond d'une struct image avec une matrice de taille 3 par 3. les pixels en bordure ne sont pas modifiés a cause de la taille
 de la matrice. Les modifications sont faites sur une copie de la structure image pour eviter que les calculs d'un pixel interferent avec ceux d'un
 pixel voisin
 Entrée(s) : l'image a modifier et le filtre(la matrice 3 par 3)
 Sortie(s) :renvoi la copie de l'image qui a été modifiée
*/
image convolution(image originalImg, int** filtre){
  //creer une copie de img
  //pr tt les pixels sauf les bords:
  //on calcul la "moyenne" pr r,g,b, et on applique ca a la copie
  image img;
  int sommeR, sommeG, sommeB;
  img=copier(originalImg);
  for (int i=1; i<img.hauteur-1;i++){
    for (int j=1; j<img.largeur-1;j++){
      sommeR=0;sommeG=0;sommeB=0;
      for (int k=0; k<3; k++){
        for (int l=0; l<3; l++){
          sommeR+=originalImg.tabPixel[i+k-1][j+l-1].r*filtre[k][l];
          sommeG+=originalImg.tabPixel[i+k-1][j+l-1].g*filtre[k][l];
          sommeB+=originalImg.tabPixel[i+k-1][j+l-1].b*filtre[k][l];
        }
      }
      if (sommeR<0){sommeR=0;}; if (sommeG<0){sommeG=0;}; if (sommeB<0){sommeB=0;};
      img.tabPixel[i][j].r=round(sommeR);img.tabPixel[i][j].g=round(sommeG);img.tabPixel[i][j].b=round(sommeB);
    }
  }
  return img;
}


/* Auteur : Simon Decostanzi
 Date :   4/06/22
 Résumé : copie une struct image dans une autre, car on ne peut pas faire img1=img2 sans que l'un soit modifié quand on modifie l'autre
 Entrée(s) : l'image a copier
 Sortie(s) : la copie de l'image
*/
image copier(image imageOriginal){
  image img;
  img.formatType=malloc(sizeof(char)*2);// car on sait que c soit "P3" soit "P6". On aurai pu juste mettre image.formatType="P3" mais c plus pro
  img.formatType="P3";
  img.hauteur=imageOriginal.hauteur;img.largeur=imageOriginal.largeur;img.luminance=imageOriginal.luminance;
  img.tabPixel=malloc(sizeof(pixel*)*imageOriginal.hauteur);
  //int tmpR1, tmpG1, tmpB1, tmpR2, tmpG2, tmpB2, tmpR3, tmpG3, tmpB3;
  for (int i=0; i<img.hauteur;i++){
    img.tabPixel[i]=malloc(sizeof(pixel)*imageOriginal.largeur);
    for (int j=0; j<img.largeur;j++){
      img.tabPixel[i][j].r=imageOriginal.tabPixel[i][j].r;
      img.tabPixel[i][j].g=imageOriginal.tabPixel[i][j].g;
      img.tabPixel[i][j].b=imageOriginal.tabPixel[i][j].b;
    }
  }
  return img;
}


/* Auteur : Simon Decostanzi
 Date :   5/06/22
 Résumé : fait la dilatation d'une image a partir d une kernel et d'une image en niveau de gris ou en binaire. les pixels en bordure ne sont pas modifiés a cause de la taille
 de la matrice. Les modifications sont faites sur une copie de la structure image pour eviter que les calculs d'un pixel interferent avec ceux d'un
 pixel voisin
 Entrée(s) : l'image a modifier, la kernel
 Sortie(s) : l'image modifiée
*/
image dilatation(image imgOriginal, int** kernel){
  image img;
  img=copier(imgOriginal);
  int max;
  for(int i=2; i<img.hauteur-2;i++){
    for(int j=2; j<img.largeur-2;j++){
      max=0;
      for(int k=0; k<5;k++){
        for(int l=0; l<5;l++){
          max=MAX(imgOriginal.tabPixel[i+k-2][j+l-2].r*kernel[k][l],max);//comme r=g=b, on prend r de maniere arbitraire
        }
      }
      img.tabPixel[i][j].r=max; img.tabPixel[i][j].g=max; img.tabPixel[i][j].b=max;
    }
  }
  return img;
}

/* Auteur : Simon Decostanzi
 Date :   5/06/22
 Résumé : meme principe que la dilatation, sauf que dans la dilatation on cherche la plus grande valeur de luminance et ici on cherche
 la plus petite
 Entrée(s) : l'image a modifier, la kernel
 Sortie(s) : l'image modifiée
*/
image erosion(image imgOriginal, int** kernel){
  image img;
  img=copier(imgOriginal);
  int min,minTmp;
  for(int i=2; i<img.hauteur-2;i++){
    for(int j=2; j<img.largeur-2;j++){
      min=256;
      //evidement le plus petit sera tjr 0 (etant donné qu'il y a des 0 dans la kernel), il faut donc verifier si y en a 1 parmis les resultats de la
      //matrice qui est plus grand que 0; si c le cas, ca devient le nouveau minimum
      for(int k=0; k<5;k++){
        for(int l=0; l<5;l++){
          minTmp=MIN(imgOriginal.tabPixel[i+k-2][j+l-2].r*kernel[k][l],min);
          if (minTmp!=0){min=minTmp;}
        }
      }
      printf("grand min: %d \n \n \n", min);
      if (min==256){//impossible, donc ca veut dire que tous les resultats etaient 0, donc 0 est la vraie valeur min
        min=0;
      }
      img.tabPixel[i][j].r=min; img.tabPixel[i][j].g=min; img.tabPixel[i][j].b=min;
    }
  }
  return img;
}

/* Auteur : Simon carriac
 Date :   05/06/2022
 Résumé : fais une rotation de 90° d'une image dans le sens horaire
 Entrée(s) : une structure image 
 Sortie(s) :  
*/
void rotation (image img){
    image tmp;
    tmp.tabPixel=malloc(sizeof(pixel*)*img.hauteur);
    for (int i=0; i<img.hauteur;i++){
      tmp.tabPixel[i]=malloc(sizeof(pixel)*img.largeur);
    }
    for (int i=0;i<img.hauteur;i++){
        for (int j=0;j<img.largeur;i++){
            tmp.tabPixel[i][j]=img.tabPixel[j][img.hauteur-1-i];
        }
    }
    for (int i=0;i<img.hauteur;i++){
        for (int j=0;j<img.largeur;i++){
            img.tabPixel[i][j]=tmp.tabPixel[j][i];
        }
    }
}

