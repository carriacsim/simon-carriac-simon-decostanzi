#ifndef __Info_H_
#define __Info_H_
#define N 70
#define MIN(a,b) (a >= b ? b : a)
#define MAX(a,b) (a >= b ? a : b)
struct pixel{
  int r;
  int g;
  int b;
};
typedef struct pixel pixel;

struct image{
  char* formatType;
  int largeur;
  int hauteur;
  int luminance;
  pixel** tabPixel;
};
typedef struct image image;


image chargerImage(char* nomFichier);

int sauvegarde (image img, char* nom);

int creationImage(char* nomFichier, int x, int y , int width);

void niveauGris(image img);

void binarisation(int seuil, image img);

void negatif(image img);

void miroir(image img);

image convolution(image originalImg, int** filtre);

image copier(image imageOriginal);

image dilatation(image imgOriginal, int** kernel);

int histogramme (image img);

image erosion(image imgOriginal, int** kernel);

void recadrage (image img);

void rotation (image img);

#endif
